

1. Project Name: Tu Language

2. Project Icon:

![](./tu.svg)

3. Project Homepage: [https://github.com/tu-lang/tu](https://github.com/tu-lang/tu)

4. Project Repository: [https://github.com/tu-lang/tu](https://github.com/tu-lang/tu)

5. Project Introduction: 

Tu Language: A Dynamic Compiled Programming Language for General-Purpose Scenarios

Tu Language is a dynamic compiled programming language designed for general-purpose use. The project began in 2018, was open-sourced in 2022, and has fully achieved self-hosting, meaning it can compile itself independently without external dependencies. It is currently in the trial and optimization phase.

The language was designed in response to the extremes of existing programming languages in pursuing performance and safety. For example, while Rust and C++ excel in these areas, they impose significant burdens on developers, leading to fatigue . On the other hand, dynamic languages like PHP, Python, and JavaScript offer higher development efficiency but generally suffer from poor performance and limited extensibility, often requiring C extensions to implement low-level features.

Tu Language aims to balance development efficiency, performance, and simplicity. It adopts dynamic syntax to avoid cumbersome type annotations, enabling developers to focus on business logic. For high-performance scenarios, it supports static syntax for writing efficient libraries and provides modern features like stack-based coroutines and multi-threaded garbage collection. Simplicity is another core goal: Tu Language is entirely self-sufficient, with no external dependencies. Its toolchain is fully self-hosting (including compilation, assembly, and linking) and runs flexibly on any amd64 Linux system.

Template for Project Introduction and Example Screenshots:

Dynamic Syntax Example:

![img](./1.png)

Static Syntax Example:

![img](./2.png)

Feature Syntax Example:

![img](./3.png)

In the future, the development team will focus on the following research and development directions:

- Improving the multi-threaded Future coroutine management framework
- Optimizing the performance and stability of the runtime's multi-threaded garbage collector (GC)
- Expanding and enriching package management tools and documentation
- Developing enterprise-level real-world application projects
- Designing intuitive and practical business frameworks

Tu Language warmly invites programming enthusiasts to join us in refining and enhancing the language. Together, we can optimize it, build better features, and create a programming language truly tailored for developers.

1. Project Classification:
```
Free, open-source (AGPL-3.0), general-purpose, open to community contributions  

Language Type    : General-purpose programming language, imperative language  
Tool Type        : General-purpose compilation tools  
Application Scope: General-purpose, industry-specific applications  
```
2. Contact Information:
Email: 97404667@qq