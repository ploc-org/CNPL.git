
1. 项目名称：DeepLang

2. 项目图标：

![logo](./logo.jpg)

3. 项目主页：[https://deeplang.org/](https://deeplang.org/)

4. 项目仓库：[https://github.com/deeplang-org/deeplang](https://github.com/deeplang-org/deeplang)

5. 项目简介:

  DeepLang是一种专为资源受限场景设计的编程语言，采用静态类型和强类型特性，语法风格参考C-style设计，并支持过程式、逻辑式和函数式的混合编程范式。面向物联网（IoT）应用，DeepLang语言具备鲜明的内存安全特性，其设计借鉴了Rust的安全机制，并针对资源受限场景的特点选择了更合适的编译与执行模式。

  DeepLang的软件体系包括编译器Deepc和虚拟机DeepVM。Deepc由OCaml开发，实现了多阶段的代码处理流程：首先通过语法解析器（parser）生成语法树，然后使用遍历器（walker）多次遍历语法树，构建符号表。转换模块（conversion）根据符号表将语法树转译为ANF IR（行政范式中间表示），最后通过codegen模块将ANF IR转换为WASM字节码。目前，Deepc仅支持类型检查器（type checker），尚未实现类型推断器（type infer），因此所有DeepLang源码必须显式标注类型，否则会被视为语法错误。DeepVM由C语言开发，支持WASM 1.0，具备字节码加载、内存管理、解释执行和FFI机制等功能。

  目前，DeepLang团队主要由来自江南大学、帝国理工学院、浙江大学以及中国科学技术大学的硕士和博士研究生组成，专注于研究资源受限场景下语言特性的设计。由于团队精力和资源有限，DeepLang现阶段尚未具备任何商用落地能力。

  DeepLang的ADT、接口、模式匹配的语言特性示例。

  ADT特性示例：
  ![adt](./adt.jpg)

  ​	接口特性示例：

![interface](./interface.jpg)

​		模式匹配特性示例：

![pattern match](./pattern_match.jpg)

6. 项目分类：

  免费、开源（MIT）、通用、接受社区贡献  
  语言类别：一般编程语言  
  工具类别：一般编译工具  
  应用领域：通用 


7. 联系方式：swubear@163.com

