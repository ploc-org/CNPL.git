
1. 项目名称：GödelScript

2. 项目图标：

![logo](./godel.svg)

3. 项目主页：[https://github.com/codefuse-ai/CodeFuse-Query/blob/main/godel-script/README.md](https://github.com/codefuse-ai/CodeFuse-Query/blob/main/godel-script/README.md)

4. 项目仓库：[https://github.com/codefuse-ai/CodeFuse-Query/tree/main/godel-script](https://github.com/codefuse-ai/CodeFuse-Query/tree/main/godel-script)

5. 项目简介：

```rust
@output
pub fn hello(greeting: string) -> bool {
    return greeting = "hello world!"
}
```

GödelScript 设计于原蚂蚁集团 CodeInsight 团队：陈欣予、范刚、傅先进、梁义南、李皓琨、李世杰、时清凯、王文洋、肖枭、周金果、甄羿等成员（首字母排序）。

GödelScript 是 CodeQuery 用于查询和数据处理的领域专用语言 (DSL)。底层引擎为 Soufflé Datalog。GödelScript 使用了类 Rust 的语法，提供了严格的类型检查、方便快捷的类型推导、智能友好的错误提示信息，使用户能够快速上手。

GödelScript 编译器主要应用场景为：

- 面向用户编写简单或复杂查询，提供更便捷的写法，提高编写查询的效率；
- 提供严格类型检查与类型推导，给予更智能的代码修改提示；
- 提供严格的 ungrounded(未赋值/未绑定) 检测，避免触发 Soufflé Ungrounded Error；
- Language Server 以及 IDE Extension 支持。

由于是基于 Datalog 的 DSL(领域专用语言)，该语言提供了一些具有 Datalog 特征的特性，如：

```rust
// create schema Person
schema Person {
    name: string,
    age: int,
}

impl Person {
    // define universal set of Person in special method __all__
    pub fn __all__() -> *Person {
        yield Person { name: "John", age: 18 };
    }

    pub fn getName(self) -> string {
        return self.name;
    }

    pub fn getAge(self) -> int {
        return self.age;
    }
}

// create schema Student extends Person
// all methods except __all__ are inherited
schema Student extends Person {}

impl Student {
    pub fn __all__() -> *Student {
        yield Student { name: "Johnson", age: 18 };
    }
    // getName inherited, also can be overridden
    // getAge inherited, also can be overridden
    pub fn getAge(self) -> string {
        return "age: " + (self.age + 1).to_string();
    }
}
```

而获取查询结果的函数可以写为：

```rust
@output
pub fn student_name(name: string) -> bool {
    for (stu in Student()) {
        return name = stu.getName();
    }
}
```

或者使用 SQL-like 语法：

```sql
query student_name from
    stu in Student()
select name = std.getName()
```

6. 项目分类：免费、开源（Apache-2.0）、通用、接受社区贡献  
语言类别：领域专用语言、声明式语言、Datalog  
工具类别：编译工具、代码生成、解释器  
应用领域：代码静态分析、数据库查询、行业应用  

7. 联系方式：lhk101lhk101@qq.com（电子邮箱）
