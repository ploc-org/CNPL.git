
1. 项目名称：CovScript 智锐编程语言

2. 项目图标

![](./covariant_script_wide.png)

3. 项目主页：[https://covscript.org.cn](https://covscript.org.cn)

4. 项目仓库：[https://github.com/covscript](https://github.com/covscript)

5. 项目简介

Covariant Script编程语言，简称CovScript，中文名简称智锐编程语言，最初发布于2017年，是一门跨平台、开放源代码的动态类型应用层通用编程语言，具有高效、易学、易用、可靠的特点，融合了现代编程语言的优点，可以通过CNI高效地与C++直接交互。

CovScript编程语言是国内首批投入市场的自主知识产权编程语言之一，具有独立、完善的工具链，包括基础解释器、调试器、实时编译器（JIT Compiler）、标准库、扩展库、文档和IDE插件等，不依附于现有编程语言运行时环境。自主、独立、完善且可靠的语言及附属生态使CovScript广受客户好评，目前已经在四川大学信息化建设与管理办公室、四川大学华西大数据中心等单位落地，7x24小时服务于关键系统中。

![](./Code.png)

CovScript编程语言是以命令式为主体，面向对象和函数式为辅的多范式编程语言，对于初学者来说简单易懂、符合直觉，解决大型项目的需求也能游刃有余。目前CovScript已有数个成熟的开发框架：

 + CovAnalysis：性能比肩Pandas的数据分析、处理框架
 + CSDBC：基于ODBC的数据库连接件，兼容绝大多数主流RDBMS
 + ParserGen：基于类EBNF规则的实时语法分析器生成器，
   + CovScript基于此实现了完全自举

除此之外，CovScript还有完善的包管理器和无数协助开发的工具库，能够帮助用户高效的满足大多数云原生应用的需求。

![](./EcoSystems.png)

CovScript虽然是一门动态编程语言，但其核心Runtime是由高度优化的C++代码编写而成，其执行速度高达900万行代码每秒，协程的上下文切换速度更是达到80 GOPS（每秒进行的十亿次操作数），能够高效地支持各类应用需求。

作为一门由中国人主导设计、开发的编程语言，CovScript更是以自身行动践行“中国智造”，其语言核心生态拥有100%自主知识产权（已在中华人民共和国国家版权局注册，登记号：2020SR0408026；已被Zenodo检索，DOI号：10.5281/zenodo.10471188），周边生态100%开源、可信。不仅如此，CovScript还完全支持国产生态：

 + 针对龙芯架构和国产操作系统专门优化、测试
 + CovScript每个发行版都会有对应的龙芯版（UOS@3A4000）
 + 兼容华为鲲鹏处理器和openEuler操作系统
 + CSDBC兼容华为openGauss数据库

为了给尽可能多的客户提供服务，CovScript还兼容存量系统。除了主流版本兼容 Windows 7 64bit，还可定制兼容 Windows XP SP2。

2017年至2022年是CovScript快速发展的五年，这五年使CovScript具备了比肩成熟编程语言的非常成熟的生态。2023年后，CovScript开始计划向现代编程语言和前沿应用领域进军，先是完善了第四代语言标准（CovScript 4，或称为ECS），后是验证了CovTorch机器学习框架的可行性。未来，CovScript将重点着力于建设LLMOps流程中的关键基础设施，为前沿应用赋能。

6. 项目分类

免费、开源（Apache 2.0）、通用、接受社区贡献  

语言类别：  
e. 命令式语言（Imperative Languages）  
l. 多范式语言（Multiparadigm Languages）  

工具类别：  
b. 解释器（Interpreters）  
e. 实时编译器（Just-in-time Compilers）  
i. 运行时环境（Runtime Environment）

应用领域：  
a. 通用（General Computation）

7. 联系方式（至少填一项，括号内注明类型）

mikecovlee（微信）、[mikecovlee@163.com](mailto:mikecovlee@163.com)（电子邮箱）
