1. Project Name: XLang

2. Project Icon：


![logo](./XLang3-small.png)


3. Project Homepage: [https://nop-platform.github.io/projects/nop-entropy/docs/dev-guide/xlang/](https://nop-platform.github.io/projects/nop-entropy/docs/dev-guide/xlang/)

4. Project Repository: [https://github.com/entropy-cloud/nop-entropy](https://github.com/entropy-cloud/nop-entropy)

5. Project Introduction:

```xml
<c:log info="hello world" />
```

XLang is a scripting language that combines XML tag syntax and JavaScript syntax. It was designed by canonical. The XLang language is one of the underlying technologies of the Nop low-code platform and is the world's first programming language with built-in support for Delta merge operators from reversible computation theory.

The Nop platform adopts the language-oriented programming paradigm (Language Oriented Programming). When developing applications, it does not directly use general-purpose programming languages (such as Java, C#) for development, but first defines a domain-specific language (DSL), and then uses the DSL to express business. In order to quickly develop and expand the DSL language, we need a meta-model language that can define the DSL. syntax structure, as well as a series of mechanisms to quickly implement the DSL interpreter, syntax-guided translation, etc. The XLang language XLang includes sub-languages such as the XDef meta-model definition language, the Xpl template language, the XScript expression language, and the XTransform structural transformation language. They together form a complete DSL development infrastructure. By adding a simple XDef meta-model definition, we can automatically obtain the corresponding DSL parser, validator, IDE plugin, debugging tool, etc., and automatically add general language features such as module decomposition, delta customization, and meta-programming to the DSL domain language.

## Defining a new DSL through XDef meta-model definition

```xml

<state-machine initial="!var-name" stateProp="!string" ignoreUnknownTransition="!boolean=false"
               xdef:name="StateMachineModel" xdef:bean-package="io.nop.fsm.model"
               x:schema="/nop/schema/xdef.xdef" xmlns:x="/nop/schema/xdsl.xdef" xmlns:xdef="/nop/schema/xdef.xdef"

    ...
    <state id="!var-name" xdef:unique-attr="id" xdef:ref="StateModel"/>

    <on-entry xdef:value="xpl"/>

    <on-exit xdef:value="xpl"/>

    <handle-error xdef:value="xpl-fn:(err)=>boolean"/>
</state-machine>
```

## Interweaving of XML and Expression Syntax

XLang does not use JSX syntax to implement XML-like syntax, but continues to use XML syntax and extends the Template expression syntax in JavaScript.

```javascript
let resut = xpl `<my:MyTag a='1' />`
const y = result + 3;
```

Equivalent to

```xml
<my:MyTag a='1' xpl:return="result" />
<c:script>
  const y = result + 3;
</c:script>
```

The overall structure of XLang strictly complies with the XML format requirements, so it meets the homoiconicity priciple first introduced by Lisp language when used as a template language to generate XML, making it particularly suitable for meta-programming and macro function implementation.

XLang modifies the parsing format of the Template expression syntax in JavaScript, recognizing the content between backtick characters as a string to be parsed at compile time, rather than an Expression list. This allows XLang to use this syntax form to extend support for more DSL formats, such as introducing a LinQ syntax similar to C#

```javascript
const result = linq `select sum(amount) from myList where status > ${status}`
```

Implementing a parser similar to LinQ syntax is very simple, just define a static function in Java, marked with @Macro

```javascript
    @Description("Compile and execute xpl language fragments, outputMode=none")
    @Macro
    public static Expression xpl(@Name("scope") IXLangCompileScope scope, 
        @Name("expr") CallExpression expr) {
        return TemplateMacroImpls.xpl(scope, expr);
    }
```

## Macro Functions and Compile-Time Execution

XLang supports compile-time meta-programming, allowing Turing-complete code to be executed at compile time and dynamically generating new syntax structures to be compiled.

```xml

<![CDATA[
import io.nop.biz.lib.BizValidatorHelper;

let validatorModel = BizValidatorHelper.parseValidator(slot_default);
// Get the corresponding abstract syntax tree
let ast = xpl <c:ast>
    <c:script>
      import io.nop.biz.lib.BizValidatorHelper;
      if(obj == '$scope') obj = $scope;
      BizValidatorHelper.runValidatorModel(validatorModel,obj,svcCtx);
    </c:script>
   </c:ast>
// Replace the identifier names in the abstract syntax tree with the model objects obtained at compile time. This way, there is no need to dynamically load and parse the model at runtime
return ast.replaceIdentifier("validatorModel",validatorModel);
]]>
```

## XDSL's Delta Generation and Merge Mechanism

All DSLs in the Nop platform support the x-extends delta merge mechanism, which implements the computational pattern required by reversible computation theory

```
App = Delta x-extends Generator
```

Specifically, all DSLs support `x:gen-extends` and `x:post-extends`
configuration sections, which are Generators executed at compile time, using the XPL template language to dynamically generate model nodes, allowing multiple nodes to be generated at once, and then merged in order, with the specific merge order defined as follows:

```
<model x:extends="A,B">
    <x:gen-extends>
        <C/>
        <D/>
    </x:gen-extends>

    <x:post-extends>
        <E/>
        <F/>
    </x:post-extends>
</model>
```

The merge result is

```
F x-extends E x-extends model x-extends D x-extends C x-extends B x-extends A
```

The current model will overwrite the results of `x:gen-extends` and `x:extends`, while `x:post-extends` will overwrite the current model.

With the help of `x:extends` and `x:gen-extends`
We can effectively implement the decomposition and combination of DSL. For more details, see [XDSL: Universal Domain-Specific Language Design](https://zhuanlan.zhihu.com/p/612512300)

## Extensible Syntax

Similar to Lisp language, the syntax of XLang can be extended through macro functions and tag functions. New syntax nodes can be introduced through `<c:lib>`, and then structural transformations can be implemented within the node through macro functions and other mechanisms.

```xml
<c:lib from="/nop/core/xlib/biz.xlib" />
<biz:Validator fatalSeverity="100"
               obj="${entity}">

    <check id="checkTransferCode" errorCode="test.not-transfer-code"
           errorDescription="扫入的码不是流转码">
        <eq name="entity.flowMode" value="1"/>
    </check>
</biz:Validator>
```

`<biz:Validator>` introduces a DSL for validation. The Validator tag will use the macro function mechanism to parse the node content at compile time and translate it into an XLang Expression for execution.

6. Project Category: Free, Open Source (AGPLv3), Universal, Accepts Community Contributions
   Language Category: Universal Programming Language, Imperative and Functional Mixed Language, Extensible Language, Interpreted Scripting Language
   Tool Category: Scripting Language, Interpreter, Code Generation
   Application Field: Universal, Industry Applications

Contact Information:
   canonical_entropy@163.com (Email)
