
1. 名称：金鱼Scheme / Goldfish Scheme

2. 项目图标：

![](./logo.png)

3. 项目主页：https://gitee.com/LiiiLabs/goldfish

4. 项目仓库：[https://gitee.com/LiiiLabs/goldfish](https://gitee.com/LiiiLabs/goldfish)

5. 项目介绍：
金鱼Scheme 是一个 Scheme 解释器，具有以下特性：
+ 兼容 R7RS-small 标准
+ 提供类似 Python 的标准库
+ 小巧且快速

金鱼Scheme的设计目标是让Scheme和Python一样易用且实用。

金鱼Scheme是应用场景驱动的编程语言项目。由于墨干理工套件所使用的S7 Scheme并不能满足墨干理工套件的长远发展，我们发起了金鱼Scheme这个项目，以满足墨干内置的7万6千行历史Scheme代码的长远维护，另外，墨干中以Python语言实现的插件，比如Gnuplot绘图插件，我们也从Python实现切换到了金鱼Scheme实现。

金鱼Scheme是采用文学编程方式实现的。我们认为：大模型时代，以代码为中心的传统编程范式，会切换到以文档为中心的文学编程范式，编程的门槛会持续降低。文学编程作为一种历史悠久的编程范式会在大模型时代经历一场文艺复兴。

金鱼Scheme的文档是中文优先的，因为发起者的母语是中文。除文档之外的社区交流，比如代码提交信息、代码合并请求标题和内容、社区开发者的飞书群均鼓励使用英文。

6. 项目分类：
免费、开源（Apache License 2.0）、通用、接受社区贡献  

+ 语言类别：一般编程语言、函数式语言  
+ 工具类别：解释器
+ 应用领域：教育、科研
