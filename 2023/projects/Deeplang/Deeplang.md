
1. 项目名称：Deeplang

2. 项目图标：

![logo](./logo.jpg)

3. 项目主页：[https://deeplang.org/](https://deeplang.org/)

4. 项目仓库：[https://github.com/deeplang-org/deeplang](https://github.com/deeplang-org/deeplang)

5. 项目简介:
Deeplang语言是具有鲜明内存安全特性的面向IoT场景的语言，设计过程中参考Rust的安全机制，但又根据IoT场景的特性选择了更合适的编译执行模式。Deeplang是一种静态类型、强类型语言，参考C-style设计语法，同时支持过程式、逻辑式和函数式的混合范式。
Deeplang编译和运行的软件分别为Deepc和DeepVM。由于Deeplang的目的是探索资源受限场景下语言特性的设计，现阶段的Deepc和DeepVM只能用于语言特性研究，并不具备任何商用能力。
Deeplang的编译。Deepc是由Ocaml开发的。首先Deeplang源码经过语法解析器（parser）生成语法树，遍历器（walker）对语法树进行多次遍历获取相关的符号表，转换模块（conversion）基于符号表将语法树转译成ANF IR，最后codegen将ANF IR转化成WASM字节码。Deepc暂时只有类型检查器（type checker），没有类型推断器（type infer），无法推断类型信息，因此所有Deeplang源码需要主动标注类型信息，否则视为语法错误。
Deeplang的运行。DeepVM是由C语言开发的，支持WASM1.0，包括字节码加载、内存管理、解释执行、FFI机制。


项目简介模板，案例截图：
![adt](./adt.jpg)
![interface](./interface.jpg)
![pattern match](./pattern_match.jpg)

6. 项目分类：免费、开源（MIT）、通用、接受社区贡献  
语言类别：一般编程语言  
工具类别：一般编译工具  
应用领域：通用 


7. 联系方式：swubear@163.com

