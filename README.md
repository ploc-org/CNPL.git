# 《国产编程语言蓝皮书-2024》

《国产编程语言蓝皮书-2024》已编制完成：  
中文版PDF：[https://cdn-static.gitcode.com/doc/CNPL-2024-CHS.pdf](https://cdn-static.gitcode.com/doc/CNPL-2024-CHS.pdf)
英文版PDF：[https://cdn-static.gitcode.com/doc/CNPL-2024-EN.pdf](https://cdn-static.gitcode.com/doc/CNPL-2024-EN.pdf)

---

本仓库是《国产编程语言蓝皮书》编委会（以下简称蓝皮书编委会）工作区。

蓝皮书编委会是编程语言开放论坛（PLOC）下属的专业委员会，2024版蓝皮书编委会经 PLOC 4号提案授权成立。

2024版蓝皮书编制方案见[《国产编程语言蓝皮书-2024》编制方案](./《国产编程语言蓝皮书-2024》编制方案.md)。

---

### 2024版蓝皮书编委会编委名单
特别顾问：梁宇宁、李建忠  
策划编辑：柴树杉、陈朝臣、丁尔男、李登淳、李皓琨、李智勇、吴森、徐芳、杨海玲、杨海龙、赵普明、朱子润

---

### 《国产编程语言蓝皮书-2023》
[https://www.ploc.org.cn/ploc/CNPL-2023.pdf](https://www.ploc.org.cn/ploc/CNPL-2023.pdf)

---

